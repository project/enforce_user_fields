<?php

namespace Drupal\enforce_user_fields\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber for check that user have filled all required fields.
 */
class EnforceUserFieldsSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The enforce user fields settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $globalConfig;

  /**
   * EnforceUserFieldsSubscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(AccountProxyInterface $current_user, RouteMatchInterface $route_match, PathMatcherInterface $path_matcher, RequestStack $request_stack, MessengerInterface $messenger, ConfigFactoryInterface $config_factory) {
    $this->currentUser = $current_user;
    $this->routeMatch = $route_match;
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->globalConfig = $config_factory->get('enforce_user_fields.settings');
    $this->pathMatcher = $path_matcher;
  }

  /**
   * Check that user have filled all required fields.
   */
  public function checkForUserFields(GetResponseEvent $event) {
    $route_name = $this->routeMatch->getRouteName();
    $skip_route = in_array($route_name, [
      'entity.user.edit_form',
      'user.logout',
      'user.reset.login',
      'image.style_public',
      'image.style_private',
    ]);
    if ($this->currentUser->isAnonymous() || enforce_user_fields_user_bypass($this->currentUser) || $skip_route) {
      return;
    }
    $is_ajax = $this->requestStack->getCurrentRequest()->isXmlHttpRequest();
    if ($is_ajax) {
      return;
    }
    // Ignore paths specified in the whitelist.
    if ($pages = mb_strtolower($this->globalConfig->get('whitelist'))) {
      $path = $this->routeMatch->getRouteObject()->getPath();
      // Do not trim a trailing slash if that is the complete path.
      $path_to_match = $path === '/' ? $path : rtrim($path, '/');

      if ($this->pathMatcher->matchPath(mb_strtolower($path_to_match), $pages)) {
        return;
      }
    }
    if (!empty($_SESSION['enforce_user_fields'])) {
      $this->messenger->addMessage($this->globalConfig->get('message'), MessengerInterface::TYPE_ERROR);
      $route_params = [
        'user' => $this->currentUser->id(),
        'destination' => Url::fromRouteMatch($this->routeMatch)->toString(),
      ];
      // If a user used one-time login token, append the token to the destination.
      $session_key = 'pass_reset_' . $this->currentUser->id();
      if (isset($_SESSION[$session_key])) {
        $route_params['pass-reset-token'] = $_SESSION[$session_key];
      }
      $redirect_url = Url::fromRoute('entity.user.edit_form', $route_params);
      $event->setResponse(new RedirectResponse($redirect_url->toString()));
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForUserFields'];
    return $events;
  }

}
