<?php

namespace Drupal\enforce_user_fields\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Enforce user fields settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'enforce_user_fields_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['enforce_user_fields.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('enforce_user_fields.settings');
    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#description' => $this->t('Message to user when need to fill required fields before proceed.'),
      '#required' => TRUE,
      '#default_value' => $config->get('message'),
    ];
    $form['whitelist'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages to not enforce'),
      '#default_value' => $config->get('whitelist'),
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %user-wildcard for every user page. %front is the front page.", [
        '%user-wildcard' => '/user/*',
        '%front' => '<front>',
      ]),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Make sure to store the 'pages to not enforce' with the leading slash.
    $input = explode(PHP_EOL, $form_state->getValue('whitelist'));
    $pages = '';
    foreach ($input as $page) {
      if (!empty($page)) {
        $pages .= '/' . ltrim($page, '/') . "\n";
      }
    }
    $this->config('enforce_user_fields.settings')
      ->set('message', $form_state->getValue('message'))
      ->set('whitelist', $pages)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
