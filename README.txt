Enforce User Fields

Force users to fill any required user account fields at login.

When enabled, every time a user login it checks if any required fields in his account still need to be filled. User is not allowed to login unless he fills the required fields.

Supported Multiple Registration module.

Useful when:

    Users can register through third party services (facebook, linkedin, you name it) and required data can not be imported.
    A new required field has been added to the users profile and we need to force existing users to fill it.
    Required fields hidden on registration step, but you want to fill it before user can have access to the site.

Similar modules

    Enforce Profile Fields - for profiles.
    Profile Fields Force Filling - for D7

INSTALLATION & USE
Enable module. All others will be done automatically.
